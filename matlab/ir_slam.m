function ir_slam(varargin)
%IR_SLAM is a function for a SLAM algorithm using 2D laser data only
%   This function iteratively reads a laser-scan from a file and uses
%   scan-matching to allign this scan. It also creates a gridmap out of
%   the alligned scans.
%
%   Use TEST.CONFIG for choosing rangescan data and other options instead
%   of command line arguments. File is in CONFIGURATION directory
%
% SYNOPSIS: ir_slam()
%
% INPUT
%		configpath: (opt) a path to the configuration files. 
%			Default: './configuration'
%       savepath: (opt) an path to save files, e.g. workspace.
%			Default: '/tmp'
%       numStart: (opt) starting line number to read from FILENAME.
%			Default: 1
%       numEnd: (opt) ending line number to read from FILENAME. 
%			Default: EOF
%
	%% Basic configurations
    
	arg = inputParser;
    arg.KeepUnmatched = true;
    arg.addParameter('configpath',fullfile('.', 'configuration'));
	arg.addParameter('savepath',fullfile(filesep,'tmp'));
 
	% Add paths of subfolders for usage of functions
	addpath(genpath(pwd));
	% Clear persistent variables in functions
	clear show_gridmap;clear plot_state;clear time_left_until_done;
	    
    tmpArg = copy(arg);tmpArg.parse(varargin{:});tmpArg = tmpArg.Results;   
	
	% IR-SLAM configuration file
	slamcfg = load_ir_slam_config(tmpArg.configpath);
	% Test options
	persistent testCfg;
	if ~isequaln(testCfg,load_test_config(tmpArg.configpath))
		clear load_scan;clear check_location;
	end
	testCfg = load_test_config(tmpArg.configpath);
	
	% Initialization of ICP for Scan-Matching, one for Scan-to-Scan and the 
	% other for Scan-to-Map Scan-Matching
	[scnScn,scnMap] = load_icp_config(tmpArg.configpath);
	
	[scanFile,locationFile] = extract_files(testCfg.datafile);	% Decompress input file

    arg.addParameter('numStart',1);
    arg.addParameter('numEnd',num_scan(scanFile));

    arg.parse(varargin{:});clear tmpArg;
    arg = arg.Results;
	
	%% Initialization

	disp('Initialization of SLAM!');
	disp(['Dataset range: ' num2str(arg.numStart) ' to ' num2str(arg.numEnd)]);	
% 	disp(['Dataset number: ' num2str(arg.numStart)]);
	disp(['Test configuration: ']);
	testCfg
	
	% Initial observation i. e. the origin of the map
	% Motion/State of roboter (x,y,theta) for current timestep (i.e.
	% initial state)
	% x,y in meter and theta in degree (readability). For implementation in C/C++ use radian instead of degree
	curState = [0,0,0];	% Here initial CURSTATE, which is the origin
	
	% First three arguments are Width, Height, Resolution of GRIDMAP. 
	gridmap = Gridmap2d(testCfg.size,testCfg.size,0.06,testCfg.sensorModel);  % (width, height, resolution, update behaviour)
	locScan = load_scan(arg.numStart,scanFile,arg.configpath);
    
	trajectory = zeros(arg.numEnd-arg.numStart+1,3);	% Save states for visualization of robot trajectory
	dTrajectory = zeros(size(trajectory,1),2*size(trajectory,2));	% Like trajectory but for DODOSTATE and DALGSTATE
	    
	% Initial map update 
	locScanTrans = locScan;
	lstScan = locScan;
  
	[freeCell] = gridmap.update(locScanTrans,curState(1:2));
	
	% Initial Occupancy Gridmap visualization
	visualization(curState,[0,0,0],locScanTrans,gridmap,trajectory,1,arg.numStart,arg,freeCell,dTrajectory,testCfg);
	if ~check_location(arg.numStart,curState,testCfg,locationFile)
		error('Location highly different from expected value!');
    end
    
	disp(['Initialization done!' char(10)]);

	%% SLAM main loop

	% Every iteration of the main loop one new scan is used
	% i.e. Observation of the next timestep
	for numScan=arg.numStart+1:arg.numEnd

        % Initialize observation for this iteration of the main loop
		tic;	% Start estimating time left until main loop done
		iter = numScan - arg.numStart + 1;	% Use for relative instead absolute indexing
        locScan = load_scan(numScan);

		% Slider for controlling visualization settings
        if ~exist('sldRun','var')
            set(figure(5),'Units','Normalized','Position',[0.8 0.05 0.2 0.15]);
            txtRun = uicontrol(figure(5),'Style','text','String','Run Speed','Units','Normalized','Position',[0.1 0.36 0.2 0.15]); 
            sldRun = uicontrol(figure(5),'Style','slider','Min',0,'Max',3,'Value',testCfg.runSpeed,'Units','Normalized','Position',[0.3 0.4 0.6 0.15]);
            txtZoom = uicontrol(figure(5),'Style','text','String','Zoom','Units','Normalized','Position',[0.1 0.21 0.2 0.15]);               
            sldZoom = uicontrol(figure(5),'Style','slider','Min',1,'Max',100,'Value',testCfg.zoom,'Units','Normalized','Position',[0.3 0.25 0.6 0.15]); % Runtime changing visualization zoom of map
            txtDec = uicontrol(figure(5),'Style','text','String','Decimation','Units','Normalized','Position',[0.1 0.06 0.2 0.15]); 
            sldDec = uicontrol(figure(5),'Style','slider','Min',1,'Max',100,'Value',testCfg.gridmapDecimation,'Units','Normalized','Position',[0.3 0.1 0.6 0.15]); % Runtime changing visualization update rate of map
        end
        testCfg.runSpeed = round(sldRun.Value);
        testCfg.zoom = sldZoom.Value;
        testCfg.gridmapDecimation = round(sldDec.Value);
        %% State Estimation (Rotation,Translation)    

        % Estimate motion from last and current/local scan i.e. use scan-matching as replacement
        % for odometry of wheels
		odoT = scnScn.icp(lstScan',locScan');
		dOdoState = affine2xytheta(odoT);
		dOdoState(1:2) = transform(dOdoState(1:2),[0 0 curState(3)]);	% Rotate translation from local to world coordinate system
   
        % Transform to (approximated) world coordinate system (gridmap)
        % via usage of the last state and scan-matching
        % result, then estimate state correction by allignment to gridmap
		locScanTrans = transform(locScan,curState+dOdoState);
		[xLim,yLim] = gridmap.mapLimit(locScanTrans);
		wndMap = gridmap.cellInRectangle(xLim,yLim);	% Reduce points to a possible box area instead of whole global map
		alnT = scnMap.icp(wndMap',locScanTrans'); 
		dAlnState = affine2xytheta(alnT);
		
        %% State Update

        dState = dOdoState+dAlnState;	% Estimated state change from last to current state
		% Do only trust Scan-Matching odometry if DSTATE seems to be to big 
		if norm(dState(1:2)) > slamcfg.maxDeltaTranslation || abs(dState(3)) > slamcfg.maxDeltaRotation
			dState = dOdoState;
		end
        curState = curState+dState;

        %% Map Update

        % Save the current as last scan for scan-matching in the next iteration
        lstScan = locScan;
		% Save states for visualization of robot trajectory. Not needed in
		% C/C++ implementation because only implementing 'Online SLAM'
		trajectory(iter,:) = curState;
		dTrajectory(iter,:) = [dOdoState,dAlnState];
	
		locScanTrans = transform(locScan,curState);

		try
			[freeCell] = gridmap.update(locScanTrans,curState(1:2));
		catch ME
			visualization(curState,dState,locScanTrans,gridmap,trajectory,iter,numScan,arg,freeCell,dTrajectory,testCfg,true);
			warning('Unexpected error in GRIDMAP.UPDATE occured!');
			rethrow(ME);
		end
		
		%% Visualization and checks for validity of estimated state and map (only for testing purposes). Also runtime checks		
		
		% Only visualize every TESTCFG.GRIDMAPDECIMATION updated map
		visualization(curState,dState,locScanTrans,gridmap,trajectory,iter,numScan,arg,freeCell,dTrajectory,testCfg);                		
		[~,runtime] = time_left_until_done(numScan,iter);
		
		if ~check_location(numScan,curState)
			disp('Stopping main loop because of location error!');
			break;
		end
		
	end		% Set here conditional breakpoint if analyzing specific iteration
    disp('SLAM main loop done!');
	%% Some additional work after end of main loop
	    
	disp(['Time average per iteration: ' datestr(runtime/(numScan-arg.numStart)/(24*60*60),'SS.FFF') ' seconds']);
	save(fullfile(arg.savepath,'workspace'));
	visualization(curState,dState,locScanTrans,gridmap,trajectory,iter,numScan,arg,freeCell,dTrajectory,testCfg,true);
end



function [scanfile,locationfile] = extract_files(datafile)
%EXTRACT_FILES gets names of data files
	scanfile = fullfile(datafile,'range.dsv');
	locationfile = fullfile(datafile,'location.dsv');		
end

function numScan = num_scan(filename)
%NUM_SCAN computes the number of lines in FILENAME
%
% SYNOPSIS: numScan=num_scan(filename)
%
% INPUT filename: the path to a file with laser scans in each line.
%
% OUTPUT numScan: number of lines in FILENAME
%
	[~,result] = system(['wc -l ',filename]);	% Unix: Count number of lines/scans
	result = strsplit(result); result = result{1};
	numScan = str2double(result);	% read until end of file if no end specified
end

function disp_state(curState,dState,numScan,arg)
%DISP_STATE prints CURSTATE and DSTATE to stdout.
	disp(['Dataset number: ' num2str(numScan) ' from ' num2str(arg.numEnd)]);	% Print scannumber of this loops observation
	disp([char(9) '(x, y, theta):' char(9) num2str(curState)]);
	disp([char(9) '(dx, dy, dtheta):' char(9) num2str(dState)]);
end

function visualization(curState,dState,locScanTrans,gridmap,trajectory,iter,numScan,arg,freeCell,dTrajectory,testCfg,forceVisu)
	if testCfg.runSpeed == 3
		if mod(iter,testCfg.gridmapDecimation) == 0
			disp_state(curState,dState,numScan,arg);
			show_gridmap(locScanTrans,gridmap,trajectory(1:iter,:),numScan,arg.numEnd,freeCell,true,testCfg.zoom);
			plot_state(dTrajectory,numScan,numScan);
		end
    elseif testCfg.runSpeed == 2
		if mod(iter,testCfg.gridmapDecimation) == 0
			show_gridmap(locScanTrans,gridmap,trajectory(1:iter,:),numScan,arg.numEnd,freeCell,true,testCfg.zoom);
		end
	elseif testCfg.runSpeed == 1
		if mod(iter,testCfg.gridmapDecimation) == 0
			show_gridmap(locScanTrans,gridmap,trajectory(1:iter,:),numScan,arg.numEnd,freeCell,false,testCfg.zoom);
		end	
	end
	if exist('forceVisu','var') && forceVisu
		disp_state(curState,dState,numScan,arg);
		show_gridmap(locScanTrans,gridmap,trajectory(1:iter,:),numScan,arg.numEnd,freeCell,true,testCfg.zoom);
		plot_state(dTrajectory,numScan,numScan);		
	end
end
