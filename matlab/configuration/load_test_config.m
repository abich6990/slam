function [testCfg] = load_test_config(filepath)
%LOAD_TEST_CONFIG loads the configuration file for usage in ICP
%
% SYNOPSIS: [testCfg]=load_test_config(filepath)
%
% INPUT filepath: the path in which a file named "test.config" lies.
%
% OUTPUT testCfg: structure for setting run and test settings
%
  file = 'test.config';
	testCfg = readconfig(fullfile(filepath,file));
        
	% Do FASTRUN and/or ACTIVATEALL if set
    testCfgField = fieldnames(testCfg);
	start = find(ismember(testCfgField,'runSpeed'));
	for j=start+1:numel(testCfgField)
		field = char(testCfgField(j));
		testCfg.(field) = testCfg.(field) && testCfg.fastRun;
	end
end