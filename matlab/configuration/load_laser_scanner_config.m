function [lscfg] = load_laser_scanner_config(filepath)
%LOAD_LASER_SCANNER_CONFIG loads the configuration file for usage in ICP
%
% SYNOPSIS: [lscfg]=load_laser_scanner_config(filepath)
%
% INPUT filepath: the path in which a file named "laser_scanner.config" lies.
%
% OUTPUT lscfg: structure for usage in LOAD_SCAN when loading scans
%
	file = 'laser_scanner.config';
	lscfg = readconfig(fullfile(filepath,file));
end