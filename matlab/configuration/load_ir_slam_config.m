function [slamcfg] = load_ir_slam_config(filepath)
%LOAD_IR_SLAM_CONFIG loads the configuration file for usage of slam
%
% SYNOPSIS: [slamcfg]=load_ir_slam_config(filepath)
%
% INPUT filepath: the path in which a file named "laser_scanner.config" lies.
%
% OUTPUT slamcfg: structure for usage in IR_SLAM when validating DSTATE
%
	file = 'ir_slam.config';
	slamcfg = readconfig(fullfile(filepath,file));
end
