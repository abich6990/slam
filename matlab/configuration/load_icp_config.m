function [scnScn,scnMap] = load_icp_config(filepath)
%LOAD_ICP_CONFIG loads the configuration file for usage in ICP
%
% SYNOPSIS: [scnScn,scnMap]=load_icp_config(filepath)
%
% INPUT filepath: the path in which a file named "icp.config" lies.
%
% OUTPUT scnScn: structure for usage in ICP for Scan-to-Scan Scan-Matching
%        scnMap: structure for usage in ICP for Scan-to-Map Scan-Matching
%
	file1 = 'icp_scnscn.config';
	file2 = 'icp_scnmap.config';
	scnScnCfg = readconfig(fullfile(filepath,file1));
	scnMapCfg = readconfig(fullfile(filepath,file2));
	
	scnScn = ICP(scnScnCfg);
	scnMap = ICP(scnMapCfg);
end