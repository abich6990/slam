function config = readconfig(filename)
%READCONFIG prepares a configuration file as a matrix of key-value pairs.
	fid = fopen(filename);
  
	settingValuePair = cell(2,1);
	
	ind = 1;
	while ~feof(fid)
		line = strtrim(fgetl(fid));
		if not(isempty(line) || all(isspace(line)) || strncmp(line,'%',1))	% Ignore empty lines and comments (starting with "%")
		  stgVal = strsplit(line,'=');
		  settingValuePair{1}{ind} = stgVal(1);
		  if isnan(str2double(stgVal(2)))
			settingValuePair{2}{ind} = stgVal(2); 
		  else
			settingValuePair{2}{ind} = str2double(stgVal(2)); 
		  end
		  ind = ind+1;    
		end
	end  
  
	settingValuePair{1} = settingValuePair{1}';
 	settingValuePair{2} = settingValuePair{2}';
   
	fclose(fid);
	
	for j = 1:size(settingValuePair{1},1)
		setting = char(settingValuePair{1}{j});
		if isnumeric(settingValuePair{2}{j})
			value = settingValuePair{2}{j};
		else
			value = char(settingValuePair{2}{j});			
		end
		config.(setting) = value;
	end	
	if ~exist('config','var');	config = struct([]);end
end