function plot_state(dTrajectory,numScan,history)
%PLOT_STATE creates a figure showing the state changes from Scan-to-Scan and
%Scan-to-Map Scan-Matching with some additional data for visualization.
	showLegend = true;
	warning off;
	
	if ~exist('history','var')
		history = 100;	% Show HISTORY old values. Newly added state is not included
	end
	
	persistent fig fgm lgdDXY lgdDTheta t1;
	
	dOdoState = dTrajectory(end,1:3);
	dAlnState = dTrajectory(end,4:6);
	dState = dOdoState + dAlnState;
	
	from = max(size(dTrajectory,1) - history + 1,1);
	to = size(dTrajectory,1);
	idx = from:to;
	posIdx = numScan + idx - to;
	
	if isempty(fig)
		fig = figure(3); % State figure
		fig.Name = 'State';
% 		movegui(fig,'southwest');
		clf(fig);
		hold on;
		title('Absolute state changes for odometry and alignment');
		
		fgm{1} = plot(zeros(0,1),'r');fgm{2} = plot(zeros(0,1),'b');
		fgm{5} = plot(zeros(0,1),'g');
		fgm{3} = plot(zeros(0,1),'r');fgm{4} = plot(zeros(0,1),'b');
		fgm{6} = plot(zeros(0,1),'g');
		
		t1 = text(0,0,'');
		if showLegend
			lgdDXY = legend('Tot ||dx,dy||_2','Odo ||dx,dy||_2','Aln ||dx,dy||_2','location','northoutside');
			lgdDTheta = legend('Tot d\theta','Odo d\theta','Aln d\theta','location','southoutside');
		end
	end

	set(groot,'CurrentFigure',fig);
	delete([fgm{1:end} t1]);
    if showLegend;delete([lgdDXY lgdDTheta]);end
	
	subplot(2,1,1);
	hold on;
	odo = sqrt(sum(dTrajectory(idx,1:2).^2,2));
	aln = sqrt(sum(dTrajectory(idx,4:5).^2,2));
	fgm{5} = plot(posIdx,odo + aln,'g');
	fgm{1} = plot(posIdx,odo,'r');
	fgm{2} = plot(posIdx,aln,'b');
	if showLegend
		lgdDXY = legend('Tot ||dx,dy||_2','Odo ||dx,dy||_2','Aln ||dx,dy||_2','location','northoutside');
	axis tight

	subplot(2,1,2);
	hold on;
	odo = abs(dTrajectory(idx,3)) ;
	aln = abs(dTrajectory(idx,6));
	fgm{6} = plot(posIdx,odo + aln,'g');
	fgm{3} = plot(posIdx,odo,'r');
	fgm{4} = plot(posIdx,aln,'b');
	if showLegend
		lgdDTheta = legend('Tot d\theta','Odo d\theta','Aln d\theta','location','southoutside');	end	
	axis tight
	
	% Set axis and compute position for T1
	xPos = xlim;
	yPos = ylim;
	yMax = max(abs(yPos));
	ylim([0 yMax]);
	yPos = ylim;

	height = yPos(2)-yPos(1);
	
	t1 = text(xPos(1),yPos(1)-height*0.25,{['(dx, dy, d\theta): ' num2str(dState)]});
	
	drawnow;
	warning on;
end