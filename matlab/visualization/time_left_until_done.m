function [timeEstim,timeCur] = time_left_until_done(numScan,iter)
%TIME_LEFT_UNTIL_DONE estimates the time needed for completing IR_SLAM.
	timeLength = 50;
	persistent time timeIdx numEnd numTotal timeRun secPerDay progBar;
	
	if isempty(time)
		time = zeros(timeLength,1);
		timeRun = 0;
		timeIdx = 1;
		numEnd = evalin('caller','arg.numEnd');
		numTotal = numEnd - evalin('caller','arg.numStart') + 1;
		secPerDay = 24*60*60;
		progBar = uicontrol(figure(5),'Style','text','String','Initialization','Units','Normalized','Position',[0.1 0.6 0.8 0.3]);
	end
	
	time(timeIdx) = toc;
	timeRun = timeRun+toc;
	
	if timeIdx == timeLength;timeIdx = 1;
	else timeIdx = timeIdx+1;end
	
    % Estimate linear change of TIME i.e. does the duration of each iteration
    % increase or decrease. This improves TIMEESTIM accuracy
    numLeft = numEnd-numScan;
    tStart = mean(time(1:ceil(timeLength*2/4)));
    tEnd = mean(time(ceil(timeLength*2/4):end));
    timeLinFac = 1+(tEnd-tStart);
    
	timeEstim = mean(time)*numLeft*timeLinFac;
	timeCur = timeRun;
    
%     disp([char(9) 'Runtime:' char(9) datestr(timeCur/secPerDay,'HH:MM:SS') char(9) 'Estimated left time:',...
%         char(9) datestr(timeEstim/secPerDay,'HH:MM:SS') char(10)]);
	
	progBar.String = ['Progress: ' num2str(iter/numTotal) '%' char(10) 'Dataset number: ' num2str(numScan) ' from ' num2str(numEnd) char(10)...
		'Runtime:' char(9) datestr(timeCur/secPerDay,'HH:MM:SS') char(9) 'Estimated left time:',...
        char(9) datestr(timeEstim/secPerDay,'HH:MM:SS')];
	drawnow;
end

