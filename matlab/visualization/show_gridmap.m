function show_gridmap(locMapTrans,gridmap,trajectory,numScan,numEnd,freeCell,allPlot,zoomRange,figGrid)
%SHOW_GRIDMAP creates a figure showing the gridmap and additional data for
%visualization.

	if exist('zoomRange','var');zoom = 'fix';else zoom = ''; zoomRange = 0;end	% 'fix' or otherwise fit to occupied range
	showLegend = false;
    crtVid = false;
    showFreeCell = false;
	
	curState = trajectory(end,:);
	
	persistent fig tit lgd fgm t1 vid frame idxFrame lenFrame sld;
	
	if isempty(fig)
		if ~exist('figGrid','var')
			fig = figure(1);
		else
			fig = figGrid;
		end
		fig.Name = 'Gridmap';
% 		movegui(fig,'center');
		clf(fig);
		hold on;
        if crtVid
            vid = VideoWriter([pwd get(fig,'Name') '_'  datestr(datetime('now','Format','yyyy-MM-dd''_''HH:mm')) '.avi']);
            open(vid);
    		lenFrame = 40;frame = cell(lenFrame,1);idxFrame = 1;
		end
		
        colormap(makeColorMap([1,1,1],[0.5,0.5,0.5],[0,0,0]));
		fgm = cell(7,1);
	 	tit = title(['OG updated at Laserscan ' num2str(numScan) ' out of ' num2str(numEnd)]);	
		t1 = text(0,0,'');
		lgd = cell(1,1);
	end

	set(groot,'CurrentFigure',fig);
	delete([fgm{1:end} t1]);
    if showLegend;delete(lgd);end
    if ~showFreeCell;freeCell = zeros(0,2);end
	
	if allPlot
		[o,f,u,ov,fv]=gridmap.mapCellVertice(gridmap.occLim(:,1)',gridmap.occLim(:,2)');
		fgm{1} = patch(u(1:4,:),u(5:8,:),0.5);
		fgm{2} = patch(f(1:4,:),f(5:8,:),fv);
		fgm{3} = patch(o(1:4,:),o(5:8,:),ov);		
		freeCell = gridmap.cell2vertice(freeCell);
		fgm{4} = patch(freeCell(1:4,:),freeCell(5:8,:),'r');
	else
		[o,~,~,~,~]=gridmap.mapCell(gridmap.occLim(:,1)',gridmap.occLim(:,2)');
		fgm{1} = gobjects(0);
		fgm{2} = gobjects(0);
		fgm{3} = scatter(o(:,1),o(:,2),'ko');
		fgm{4} = gobjects(0);		
	end
    fgm{5} = plot(trajectory(:,1),trajectory(:,2),':gx','MarkerSize',6);	
	fgm{6} = plot(locMapTrans(:,1),locMapTrans(:,2),'c+');
	fgm{7} = quiver(curState(1),curState(2),cos(deg2rad(curState(3))),sin(deg2rad(curState(3))),'kd','MarkerSize',10,'LineWidth',1.5,'MarkerFaceColor','g');
 	tit.String = ['OG updated at Laserscan ' num2str(numScan) ' out of ' num2str(numEnd)];
   
    axis equal;
	zoom_fig(zoom,curState,zoomRange,gridmap);
	
	xPos = xlim;yPos = ylim;
	height = yPos(2)-yPos(1);
	t1 = text(xPos(1),yPos(1)-height*0.05,{['x: ' num2str(curState(1)) '; y: ' num2str(curState(2)) '; \theta: ' num2str(curState(3))]});
	
	if showLegend
		lgd = create_legend(fgm);
	end	
	% For video creation if it is set 
	if crtVid
		[vid,frame,idxFrame] = record_frame(vid,frame,idxFrame,fig,lenFrame,numScan,numEnd);
	end
	
	drawnow;
end

function zoom_fig(zoom,curState,zoomRange,gridmap)
	switch zoom
		case 'fix'
			% Zoom fixed i. e. follow roboter movement with a fixed size
			% camera window
			xlim([(curState(1)-1.5*zoomRange) (curState(1)+1.5*zoomRange)]);    % x axis greater because of display width usage
			ylim([(curState(2)-zoomRange) (curState(2)+zoomRange)]);
			
		otherwise
			xlim(gridmap.occLim(:,1)');
			ylim(gridmap.occLim(:,2)');
	end
end

function lgd = create_legend(fgm)
	warning off;	% Set to off because first map legend may not be ordered accurately
	fgmName = {'Unknown Cells','Free Cells','Occupied Cells','Rangebeam Cells','Trajectory',...
		   'Current Scan','Current State'};
	lgd = legend(fgmName{not(cellfun('isempty',fgm))},'location','northeastoutside');
	warning on;
end

function [vidNext,frameNext,idxFrameNext] = record_frame(vid,frame,idxFrame,fig,lenFrame,numScan,numEnd)
	if ~isempty(vid)
		frame{idxFrame} = getframe(fig);
		if idxFrame == lenFrame || numScan == numEnd
			for j = 1:idxFrame;writeVideo(vid,frame{j});end
		end
		if numScan == numEnd;close(vid);vid = [];end
		if idxFrame == lenFrame;idxFrameNext = 1;
		else idxFrameNext = idxFrame+1;end
	end
	
    frameNext = frame;
	vidNext = vid;
end
