function play_scan(numStart,numEnd,history)
%PLAY_SCAN creates a figure showing scans

	addpath(genpath(pwd));
	
	if ~exist('history','var');history = 2;end
	load(fullfile(filesep,'tmp','workspace.mat'));
	if ~exist('numStart','var');numStart = arg.numStart;end
	if ~exist('numEnd','var');numEnd = arg.numEnd;end
	
	if numEnd - numStart - history < 0 && history < 5
		error('History to large for playing scans!');
	end
	
	fig = figure(2);
	fig.Name = 'Map';
	clf(fig);

	pN = cell(history,1);
	q = quiver(0,0,0,0.5,'kd','MarkerSize',10,'LineWidth',1.5,'MarkerFaceColor','g');
	col = {'r','b','g','c'};
	
	lgd = cell(history+1,1);
	lgd(:) = {''};
	lgd{end} = 'Location';
	warning off;
	leg = legend(lgd);		
	warning on;
	
	for idx = 1:length(pN)
		pN{idx} = plot(0);
	end
	
	title(['Observation history from ' num2str(numStart) ' to ' num2str(numEnd)]);

	for absIdx = numStart+history-1:numEnd
		set(groot,'CurrentFigure',fig);
		delete([pN{:} leg q]);

		hold on;
		curNumScan = absIdx-history+1:absIdx;
		for idx = 1:length(pN)
			curScan = load_scan(curNumScan(idx),scanFile,arg.configpath);	
			pN{idx} = plot(-curScan(:,2),curScan(:,1),col{idx});	% Turn axis to change view. Top is front and bottom is rear of roboter
			lgd{idx} = num2str(curNumScan(idx));
		end
		
		q = quiver(0,0,0,0.5,'kd','MarkerSize',10,'LineWidth',1.5,'MarkerFaceColor','g');		
		axis equal;
		leg = legend(lgd);		
		drawnow;
	end
end
