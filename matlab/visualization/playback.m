function playback(varargin)
%PLAYBACK replays the last NUMFRAMES gridmap updates (only approximation) using
%known data

	addpath(genpath(pwd));

	clear show_gridmap plot_state;

    argPlayback = inputParser;
    argPlayback.KeepUnmatched = true;
    argPlayback.addParameter('workspaceFile',fullfile(filesep,'tmp','workspace.mat'));
    argPlayback.addParameter('numFrames',40);   
    argPlayback.addParameter('decimation',25);   
    argPlayback.addParameter('hqPlot',false);   
    argPlayback.addParameter('zoom','');   % 'fix' or ''
	argPlayback.addParameter('state',true);
    argPlayback.parse(varargin{:});
    argPlayback = argPlayback.Results;

	load(argPlayback.workspaceFile);
	trajectory = trajectory(1:iter,:);
	dTrajectory = dTrajectory(1:iter,:);
	
	argPlayback.numFrames = min(numScan,argPlayback.numFrames);
	absIdx = max(numScan+(-argPlayback.numFrames+1)*argPlayback.decimation,mod(numScan,argPlayback.decimation)):argPlayback.decimation:numScan;
	argPlayback.numFrames = length(absIdx);
	
	disp(['Playback from ' num2str(absIdx(1)) ' to ' num2str(absIdx(end))])
	disp('Press "s" to skip frames or "f" to pause')
	if argPlayback.state;plot_state(dTrajectory,numScan,numScan);end		

	figGrid = figure(1);
	set(figGrid,'KeyPressFcn',@(h_obj,evt) pause(0));
	
	for relIdx = 1:length(absIdx)
		if get(figGrid,'CurrentCharacter') == 's'
			set(figGrid,'CurrentCharacter','@');
			pause(0.1);
			continue;
		elseif get(figGrid,'CurrentCharacter') == 'f'
			set(figGrid,'CurrentCharacter','@');
			waitforbuttonpress;
		end
		
		locScan = load_scan(absIdx(relIdx),scanFile,arg.configpath);
		curState = trajectory(end+(-argPlayback.numFrames+relIdx)*argPlayback.decimation,:);
		locScanTrans = transform(locScan,curState);
		
		show_gridmap(locScanTrans,gridmap,trajectory(1:end+(-argPlayback.numFrames+relIdx)*argPlayback.decimation,:),...
			absIdx(relIdx),arg.numEnd,zeros(0,2),argPlayback.hqPlot,argPlayback.zoom,figGrid);
	end
end
