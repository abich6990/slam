This folder contains the matlab files for a SLAM (Simultaneous Localization and 
Mapping) project. Currently some files/functions with Copyright are used. 
Because of these files a license file is included. This file is 
named 'license.txt'.

Files with Copyright:
	'icp.m'
    'Gridmap2d.m' % only the BRESENHEM function
	
Folders contain functions for 'ir_slam.m'. To 
start the SLAM algorithm call the function 'ir_slam' from file 'ir_slam.m'. Arguments
are the start- and endfilelinenumber. Also a path for configuration files can be given as argument. 
For detailed informations on 'ir_slam' and its arguments read file 'ir_slam.m'. 
