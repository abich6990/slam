classdef Gridmap2d < handle
%GRIDMAP2D is a handle class for creating a gridmap.
%   The created gridmap is used for inserting rangebeams (laser-scans)
%   using raycasts (occlusion detection).
    
    properties (SetAccess = private)
        map
        height
        width
        resolution
						
        idxWidthOrigin
        idxHeightOrigin
		xlim
		ylim
        occLim

		logOddHit
		logOddMiss
		logOddConvergence
		
		updateCounter
		chooseSensorModel
    end
    
    methods
        function obj = Gridmap2d(height,width,resolution,sensorModel)
        %GRIDMAP2D is the constructor of its class
            if isscalar(height) && height > 0;obj.height = height; else error('height must be a positive real!'); end
            if isscalar(width) && width > 0;obj.width = width; else error('width must be a positive real!'); end
            if isscalar(resolution) && resolution > 0;obj.resolution = resolution; else error('resolution must be a positive real!'); end
            
			if exist('sensorModel','var'); obj.chooseSensorModel = sensorModel;	% 'onEvent', 'onInterval', 'raytrace', 'inverse', 'direct', otherwise error
			else obj.chooseSensorModel = 'onEvent'; end
				
			obj.updateCounter = 0;
			
			obj.logOddHit = log(0.65/(1-0.65));
			obj.logOddMiss = log(0.35/(1-0.35));
			obj.logOddConvergence = 1e2;

			% Index for accessing middle of MAP i.e. the origin
            obj.idxWidthOrigin = int32(ceil(obj.width/(2*obj.resolution)+0.5));
            obj.idxHeightOrigin = int32(ceil(obj.height/(2*obj.resolution)+0.5));
            
            obj.map = zeros(2*obj.idxHeightOrigin-1,2*obj.idxWidthOrigin-1);   % Init MAP with "unknown"
            
			lowLim = @(limIdx) (double(-limIdx)+0.5)*obj.resolution;
			highLim = @(limIdx) (double(limIdx)-0.5)*obj.resolution;
			obj.xlim = [lowLim(obj.idxWidthOrigin),highLim(obj.idxWidthOrigin)];
			obj.ylim = [lowLim(obj.idxHeightOrigin),highLim(obj.idxHeightOrigin)];
            obj.occLim = zeros(2,2);
		end
		        
        function [freeCell] = update(obj,pnt,pos)
        %UPDATE uses one or more sensor models for finding free and 
		%occupied cells and in-/decreases occupancy probability for these cells.            
			% Acquire the to changing cells of map
			[idxFree,idxOcc] = obj.mapChange(pnt,pos);
															
           % Find cells that are believed to be free space. Making them unique is
           % usually only needed for visualization of free cells
            uniqueFreeIdx = unique(idxFree,'rows');
            freeCell = obj.index2cell(uniqueFreeIdx(:,1),uniqueFreeIdx(:,2));
                        
			obj.miss([idxFree(:,1),idxFree(:,2)]);
            obj.hit([idxOcc(:,1),idxOcc(:,2)]);
                        
            % Compute map boundaries of occupied cells. This is useful for
            % knowing which part of the map is in use and limiting
            % computation for the search of occupied space
            locOccLim = [min(pnt(:,1)),min(pnt(:,2));max(pnt(:,1)),max(pnt(:,2))];
            locGt = obj.occLim < locOccLim;locLt = not(locGt);
            
            obj.occLim(2,locGt(2,:)) = locOccLim(2,locGt(2,:));
            obj.occLim(1,locLt(1,:)) = locOccLim(1,locLt(1,:));

			obj.updateCounter = obj.updateCounter + 1;			
		end
		
		function [idxFree,idxOcc] = mapChange(obj,pnt,pos)
			if strcmp(obj.chooseSensorModel,'onEvent')
				thresSim = 0.70;
				similiar = sum(obj.isOccupied(obj.point2index(pnt)))/size(pnt,1) > thresSim;
				if similiar
% 					[idxFreeBuf,idxOccBuf] = obj.inverseSensorModel(pnt,pos);					
					[idxFreeBuf,idxOccBuf] = obj.directSensorModel(pnt,pos);
				else
					[idxFreeBuf,idxOccBuf] = obj.raytraceSensorModel(pnt,pos);
				end
			elseif strcmp(obj.chooseSensorModel,'onInterval')
				modulo = 25;	% Every second (SICK S300) use other sensor model
				if mod(obj.updateCounter,modulo) == modulo-1
% 					[idxFreeBuf,idxOccBuf] = obj.inverseSensorModel(pnt,pos);					
					[idxFreeBuf,idxOccBuf] = obj.directSensorModel(pnt,pos);
				else
					[idxFreeBuf,idxOccBuf] = obj.raytraceSensorModel(pnt,pos);
				end	
			else
				if strcmp(obj.chooseSensorModel,'raytrace')
					[idxFreeBuf,idxOccBuf] = obj.raytraceSensorModel(pnt,pos);		
				elseif strcmp(obj.chooseSensorModel,'inverse')
					[idxFreeBuf,idxOccBuf] = obj.inverseSensorModel(pnt,pos);					
				elseif strcmp(obj.chooseSensorModel,'direct')
					[idxFreeBuf,idxOccBuf] = obj.directSensorModel(pnt,pos);
				else
					error(['"' obj.chooseSensorModel '"' ' is not a valid sensor model!'])
				end				
			end
			
			idxFree = cell2mat(idxFreeBuf);
			idxOcc = cell2mat(idxOccBuf);
		end
		
		function [idxFreeBuf,idxOccBuf] = raytraceSensorModel(obj,pnt,pos)
		%DIRECTSENSORMODEL assumes no range errors but error in position estimation
			idxPnt = obj.point2index(pnt);
            idxPos = obj.point2index(pos);	
			
            % Buffer for indices of free/occupied cells
			idxFreeBuf = cell(size(pnt,1),1);
            idxOccBuf = cell(size(pnt,1),1);		
			
			idxBuf = Gridmap2d.bresenham(idxPos,idxPnt);

			for row=1:size(pnt,1)
				xIdx = idxBuf{row}(:,1);
				yIdx = idxBuf{row}(:,2);
				
				occ = obj.isOccupied([xIdx,yIdx]);
				if sum(occ) == 0
					idxFreeBuf{row} = [xIdx(1:end-1),yIdx(1:end-1)];
					idxOccBuf{row} = [xIdx(end),yIdx(end)];
				else
					idxBlocked = find(occ > 0);idxBlocked = idxBlocked(1);	% First occupied cell is hit early via raycasting
					idxFreeBuf{row} = [xIdx(1:idxBlocked-1),yIdx(1:idxBlocked-1)]; % IDXBLOCKED would be the occupied cell itself. Therefore -1
					idxOccBuf{row} = [xIdx(idxBlocked),yIdx(idxBlocked)];   % Cells which are hit early via raycasting
				end
				if isempty(idxFreeBuf{row})
					idxFreeBuf{row} = int32(zeros(0,2));
				end
				if isempty(idxOccBuf{row})
					idxOccBuf{row} = int32(zeros(0,2));
				end
			end			
		end		
		
		function [idxFreeBuf,idxOccBuf] = inverseSensorModel(obj,pnt,pos)
		%INVERSESENSORMODEL assumes range errors but perfect position estimation
            rangeError = 0.03;  % 1 sigma range error of SICK S300 (range < 10m), but more or less common for laser rangefinder
			[ang,range] = cart2pol(pnt(:,1)-pos(1),pnt(:,2)-pos(2));
			rangeP = range + rangeError;
			rangeM = range - rangeError;
			[x,y] = pol2cart([ang;ang],[rangeP;rangeM]);
			pntPM = bsxfun(@plus,[x,y],pos);
	
			idxPntP = obj.point2index(pntPM(1:end/2,:));
			idxPntM = obj.point2index(pntPM(end/2 + 1:end,:));
            idxPos = obj.point2index(pos);	
			
            % Buffer for indices of free/occupied cells		
			idxFreeBuf = Gridmap2d.bresenham(idxPos,idxPntM);
			idxOccBuf = Gridmap2d.bresenham(idxPntM,idxPntP);
			
			idxFreeBuf = cellfun(@(x) x(1:end-1,:),idxFreeBuf(:),'UniformOutput',false);
		end
     		
		function [idxFreeBuf,idxOccBuf] = directSensorModel(obj,pnt,pos)
		%DIRECTSENSORMODEL assumes no range errors and perfect position estimation
			idxPnt = obj.point2index(pnt);
            idxPos = obj.point2index(pos);	
			
			idxBuf = Gridmap2d.bresenham(idxPos,idxPnt);
			
			idxFreeBuf = cellfun(@(x) x(1:end-1,:),idxBuf(:),'UniformOutput',false);			
			idxOccBuf = cellfun(@(x) x(end,:),idxBuf(:),'UniformOutput',false);			
		end		
		
        function cell = cellInRectangle(obj,xlim,ylim)
        %CELLINRECTANGLE returns all occupied cell middle points residing 
        %in the XLIM, YLIM rectangle.
			idxLim = obj.point2index([xlim',ylim']);
            
            % Get occupancy log odd probability of cells in rectangle made by XLIM
            % and YLIM
            logOdd = obj.map(idxLim(1,2):idxLim(2,2),idxLim(1,1):idxLim(2,1));
            
            % Categorize cells by occupied, free or unknown
			[occR,occC] = find(logOdd > 0);
			
            xOff = idxLim(1,1)-1;yOff = idxLim(1,2)-1;
			cell = obj.index2cell(int32(occC)+xOff,int32(occR)+yOff);			
        end		
		
        function cell = point2cell(obj,pnt)
        %POINT2CELL converts a point into its corresponding cell middle
        %point.
            idx = obj.point2index(pnt);
            cell = obj.index2cell(idx(:,1),idx(:,2));
		end

		function [occ,free,ukn,occVal,freeVal] = mapCell(obj,xlim,ylim)
        %MAPCELL returns for each celltype the cell middle position for each 
        %cell residing in the XLIM, YLIM rectangle.
			if ~exist('xlim','var');xlim = obj.xlim;ylim = obj.ylim;end
			idxLim = obj.point2index([xlim',ylim']);
            
            % Get occupancy log odd probability of cells in rectangle made by XLIM
            % and YLIM
            logOdd = obj.map(idxLim(1,2):idxLim(2,2),idxLim(1,1):idxLim(2,1));
            
            % Categorize cells by occupied, free or unknown
			[occR,occC] = find(logOdd > 0);
			occVal = obj.logOdd2probability(logOdd(sub2ind(size(logOdd),occR,occC)));
			[freeR,freeC] = find(logOdd < 0);
			freeVal = obj.logOdd2probability(logOdd(sub2ind(size(logOdd),freeR,freeC)));
			[uknR,uknC] = find(logOdd == 0);
			
            xOff = idxLim(1,1)-1;yOff = idxLim(1,2)-1;
			occ = obj.index2cell(int32(occC)+xOff,int32(occR)+yOff);
			free = obj.index2cell(int32(freeC)+xOff,int32(freeR)+yOff);
			ukn = obj.index2cell(int32(uknC)+xOff,int32(uknR)+yOff);
        end
        
        function [occ,free,ukn,occVal,freeVal] = mapCellVertice(obj,xlim,ylim)
        %MAPCELLVERTICE works like MAPCELL but returns the four vertice
        %points for each cell instead of its middle position.
			if ~exist('xlim','var');xlim = obj.occLim(:,1)';ylim = obj.occLim(:,2)';end
            
           [occ,free,ukn,occVal,freeVal] = obj.mapCell(xlim,ylim);
           occ = obj.cell2vertice(occ);
           free = obj.cell2vertice(free);
           ukn = obj.cell2vertice(ukn);
		end
		        
        function ver = cell2vertice(obj,cell)
        %CELL2VERTICE converts a cell into its 4 corresponding vertice points for visualization.
            if isempty(cell);ver=zeros(8,0);return;end
            res2 = obj.resolution/2;
            v1 = [cell(:,1)-res2,cell(:,2)-res2]';
            v2 = [cell(:,1)+res2,cell(:,2)-res2]';
            v3 = [cell(:,1)+res2,cell(:,2)+res2]';
            v4 = [cell(:,1)-res2,cell(:,2)+res2]';

            ver = [v1(1,:);v2(1,:);v3(1,:);v4(1,:);v1(2,:);v2(2,:);v3(2,:);v4(2,:)];
        end				
		
        function save(obj,filename)
        %SAVE stores an approximation of the gridmap into a file.
            % Data for reconstruction of matrix 
			fid = fopen(filename,'w');
            fprintf(fid,'height: %.2f width: %.2f resolution: %4.2f highQualityPoints: %d\n',obj.height,obj.width,obj.resolution,obj.highQualityPoints);
            fprintf(fid,'occLim: %f,%f,%f,%f\n',obj.occLim(1,1),obj.occLim(1,2),obj.occLim(2,1),obj.occLim(2,2));
            fclose(fid);
          
			% Find linear indices for occupied and free cells
			occLin = find(obj.map > 0);
			freeLin = find(obj.map < 0);
			
			% Need sorted linear indices for storage as ranges in a file. This way not
			% every elements indices must be saved
			occLin = sort(occLin);
			freeLin = sort(freeLin);
			
			% Search for ranges in linear indices i. e. slope greater 1
			occDelta = diff(occLin);
			freeDelta = diff(freeLin);
            
            occIdxEnd = find(occDelta > 1);
            freeIdxEnd = find(freeDelta > 1);
            
            occRangeStart = [occLin(1);occLin(occIdxEnd + 1)];
            freeRangeStart = [freeLin(1);freeLin(freeIdxEnd + 1)];
            occRangeEnd = [occLin(occIdxEnd);occLin(end)];
            freeRangeEnd = [freeLin(freeIdxEnd);freeLin(end)];
            
            dlmwrite(filename,int32(occRangeStart'),'-append','precision','%d');
            dlmwrite(filename,int32(occRangeEnd'),'-append','precision','%d');
            dlmwrite(filename,int32(freeRangeStart'),'-append','precision','%d');
            dlmwrite(filename,int32(freeRangeEnd'),'-append','precision','%d');
        end
        
	end
	
	methods (Static)
        function newObj = load(filename)
        %LOAD replaces this gridmap with the one stored in the file.
            fid = fopen(filename);
            dim = textscan(fgetl(fid),'%s %f %s %f %s %f %s %d','Delimiter',':');
            lim = textscan(fgetl(fid),'%s %f,%f,%f,%f');
            fclose(fid);

            range = dlmread(filename,',',2,0);
            
            occLen = nnz(range(1,:));
            freeLen = nnz(range(3,:));
            
            occRangeStart = range(1,1:occLen);
            occRangeEnd = range(2,1:occLen);
            freeRangeStart = range(3,1:freeLen);
            freeRangeEnd = range(4,1:freeLen);
            
            newObj = Gridmap2d(dim{2},dim{4},dim{6},boolean(dim{8}));
            newObj.occLim = [lim{2},lim{3};lim{4},lim{5}];
            
            % Recreate continuous range from start- and endranges. This lines
            % are not easy to understand but are computational efficient
            occLin = cumsum(accumarray(cumsum([1;occRangeEnd(:)-occRangeStart(:)+1]),[occRangeStart(:);0]-[0;occRangeEnd(:)]-1)+1);   
            occLin = occLin(1:end-1);            
            freeLin = cumsum(accumarray(cumsum([1;freeRangeEnd(:)-freeRangeStart(:)+1]),[freeRangeStart(:);0]-[0;freeRangeEnd(:)]-1)+1);   
            freeLin = freeLin(1:end-1);
            
            newObj.map(occLin) = newObj.logOddConvergence;
            newObj.map(freeLin) = -newObj.logOddConvergence;
        end
        
		function [xLim,yLim] = mapLimit(refMap)
		%MAPLIMIT searches for the minimum and maximum values of x and y in a
        %pointcloud and adds a margin to these values.
			windowOverlap = 30;
            xLim = [min(refMap(:,1))-windowOverlap,max(refMap(:,1))+windowOverlap];
			yLim = [min(refMap(:,2))-windowOverlap,max(refMap(:,2))+windowOverlap];
        end
        
        function [idxBuf] = bresenham(startIndice,endIndice)
        %BRESENHAM finds walked cell indeces from a start- to an endpoint.
        % by Aaron Wetzler, 12 Jul 2010 (Updated 15 Jul 2010) 
        % Modified by Alex Abich, Ulm University of Applied Sciences 2016
			idxBuf = cell(size(endIndice,1),1);
			if size(startIndice,1) == 1
				startIndice = repmat(startIndice,size(endIndice,1),1);
			end
		
			for row = 1:size(endIndice,1)
				curEndIndice = endIndice(row,:);
				curStartIndice = startIndice(row,:);
				x1 = curStartIndice(1); x2 = curEndIndice(1);
				y1 = curStartIndice(2); y2 = curEndIndice(2);
				dx = abs(x2-x1);dy = abs(y2-y1);
				steep = abs(dy)>abs(dx);
				if steep;t = dx;dx = dy;dy = t;end

				%The main algorithm goes here.
				if dy==0 
					q = zeros(dx+1,1);
				else
					q = [0;diff(mod([floor(dx/2):-dy:-dy*dx+floor(dx/2)]',dx))>=0];
				end
				%and ends here.
				if steep
					if y1<=y2; y = [y1:y2]';else y = [y1:-1:y2]';end
					if x1<=x2; x = x1+int32(cumsum(q));else x = x1-int32(cumsum(q));end
				else
					if x1<=x2; x = [x1:x2]';else x = [x1:-1:x2]';end
					if y1<=y2; y = y1+int32(cumsum(q));else y = y1-int32(cumsum(q));end
				end
				
				idxBuf{row} = [x,y];
			end
		end
		
		function prob = logOdd2probability(logOdd)
			prob = 1 - 1./(1+exp(logOdd));
		end		
	end
	
	methods (Access = private)
        function hit(obj,xy)
        %HIT converts (if necessary) XY to indeces and increases the
        %indexed cells hit-count by one.
            if ~isinteger(xy);idx = obj.point2index(xy);else idx = xy;end	% Check whatever XY is a point or index
			idxHit = obj.idx2linIdx(idx(:,1),idx(:,2));   % Input XY is big, therefore use linear indices
            
			% Occupancy probability has a highest and lowest value, to avoid obscure effects due to overflow
			isHitMax = obj.map(idxHit) > obj.logOddConvergence;
			idxHit(isHitMax) = [];
			
			obj.map(idxHit) = obj.map(idxHit) + obj.logOddHit;			
        end
        
        function miss(obj,xy)
        %MISS converts (if necessary) XY to indeces and increases the
        %indexed cells miss-count by one.
            if ~isinteger(xy);idx = obj.point2index(xy);else idx = xy;end	% Check whatever XY is a point or index
			idxMiss = obj.idx2linIdx(idx(:,1),idx(:,2));   % Input XY is big, therefore use linear indices
            
			% Occupancy probability has a highest and lowest value, to avoid obscure effects due to overflow
			isMissMax = obj.map(idxMiss) < -obj.logOddConvergence;
			idxMiss(isMissMax) = [];
			
			obj.map(idxMiss) = obj.map(idxMiss) - obj.logOddHit;	
        end
        
        function occ = isOccupied(obj,idx)
        %ISOCCUPIED returns indeces of occupied cells indexed by XIDX and YIDX.	
			logOdd = diag(obj.map(idx(:,2),idx(:,1)));
			occ = logOdd > 0;		
		end
 
		function free = isFree(obj,idx)
        %ISFREE returns indeces of free cells indexed by XIDX and YIDX.
			logOdd = diag(obj.map(idx(:,2),idx(:,1)));
			free = logOdd < 0;		
		end
		
        function ukn = isUnknown(obj,idx)
        %ISOUNKNOWN returns indeces of unknown cells indexed by XIDX and YIDX.
			logOdd = diag(obj.map(idx(:,2),idx(:,1)));
			ukn = logOdd == 0;		
        end

        function [dyn,dynVal] = isDynamic(obj,xIdx,yIdx)
        %ISDYNAMIC returns indeces of likely dynamic cells indexed by XIDX and YIDX.
        %There is a need that the input indices are refering to endpoints
        %of a laserscan. Otherwise the output is not valid.
            dynVal = obj.occupancyProbability(xIdx,yIdx);
            dyn = dynVal < obj.dynamicThres;
		end
        		
        function occProb = occupancyProbability(obj,xIdx,yIdx)
        %OCCUPANCYPROBABILITY returns the cells probabilty of being occupied.
			occProb = diag(obj.map(yIdx,xIdx));
			occProb = Gridmap2d.logOdd2probability(occProb);
        end
        
        function cell = index2cell(obj,xIdx,yIdx)
        %INDEX2CELL converts an index into its corresponding cell middle
        %point.
            if isempty(xIdx) ;cell = zeros(0,2);return;end
            xIdxOff = double(xIdx-obj.idxWidthOrigin);
            yIdxOff = double(yIdx-obj.idxHeightOrigin);
            cell = [xIdxOff*obj.resolution,yIdxOff*obj.resolution];
        end
        
        function idx = point2index(obj,pnt)
		%POINT2INDEX converts a point into its corresponding index.
            if isempty(pnt);idx = zeros(0,2);return;end
            idx = int32(round(pnt/obj.resolution));
            idx(:,1) = idx(:,1)+obj.idxWidthOrigin; % Correct offset of index to origin
            idx(:,2) = idx(:,2)+obj.idxHeightOrigin;    % Correct offset of index to origin
        end		
		                
		function linIdx = idx2linIdx(obj,xIdx,yIdx)
		%IDX2LINIDX converts an index into its corresponding linear index.
		%This is useful when indexing only some elements in a matrix.
            linIdx = sub2ind(size(obj.map),yIdx,xIdx);
        end
	end
end

