function [pnt2dTrans] = transform(pnt2d,xytheta)
%TRANSFORM uses a 3x1 rowvector to transform 2D cartesian points.
%
% SYNOPSIS: [pnt2dTrans]=transform(pnt2d,xytheta)
%
% INPUT pnt2d: 2D points to transform. nx2 matrix.
%       xytheta: 3x1 rowvector holding translation x, y and rotation theta.
%
% OUTPUT pnt2dTrans: nx2 matrix with transformed points.
%
	if isempty(pnt2d)
		pnt2dTrans = [];
		return;
	end
	
	affine = xytheta2affine(xytheta);
	pnt2d = [pnt2d'; ones(1,size(pnt2d,1))];
	
	pnt2dTrans = [affine*pnt2d]';
	pnt2dTrans(:,end) = [];
end