function [cartesian] = load_scan(numScan,filename,configpath)
%LOAD_SCAN reads a line from a file which holds distance values and
%converts them into x, y coordinates.
%
% SYNOPSIS: [cartesian]=load_scan(numScan,filename)
%
% INPUT numScan: the line number to read.
%       filename: the file to read from.
%
% OUTPUT cartesian: nx2 matrix holding x, y coordinates.
%
    persistent lscfg reading angle;
	
	if isempty(lscfg) && exist('filename','var') && exist('configpath','var')
		lscfg = load_laser_scanner_config(configpath);
		
		[~,result] = system(['wc ',filename]);	% Unix: Count number of lines/scans and range/angle records
		result = strsplit(result);
		numScans = str2double(result{2});
		numRecords = str2double(result{3});
		scanLength = numRecords/numScans;
		
		fileID = fopen(filename);
		data = textscan(fileID,'%f','Delimiter',':','CollectOutput',true);
		data = data{1};
		fclose(fileID);

		reading = reshape(data(1:2:end),scanLength,length(data(1:2:end))/scanLength);
		angle = data(2:2:end);
		angle = reshape(deg2rad(angle),scanLength,length(data(2:2:end))/scanLength);
	end
	
	range = reading(:,numScan);
	phi = angle(:,numScan);
	% Convert data from polar to cartesian
	polar = [range phi];
	polar(range >= lscfg.maxRange,:) = [];	% Remove non valid range meassurements 

	[X,Y] = pol2cart(polar(:,2),polar(:,1));
	cartesian = [X Y];
end

