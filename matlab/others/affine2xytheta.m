function xytheta = affine2xytheta(trans)
%AFFINE2XYTHETA converts a homogeneous 3x3 matrix into translation and
%rotation.
%
% SYNOPSIS: xytheta=affine2xytheta(trans)
%
% INPUT trans: 3x3 homogeneous matrix to convert into scalar values.
%
% OUTPUT xytheta: rowvector composed of translation x, y and orientation
% theta.
%
	theta = asin(trans(2,1));	% theta = sin(); where THETA is in radian
	theta = rad2deg(theta);
	
	xytheta = [trans(1,3),trans(2,3),theta];
end
