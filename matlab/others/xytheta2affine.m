function T = xytheta2affine(xytheta)
%XYTHETA2AFFINE converts a rowvector 3x1 into a homogeneous 3x3 matrix.
%
% SYNOPSIS: T=xytheta2affine(xytheta)
%
% INPUT xytheta: 3x1 rowvector with x, y and theta.
%
% OUTPUT T: homogeneous 3x3 matrix.
%
	t = xytheta(1:2)';
	deg = xytheta(3);
	R = [cos(deg2rad(deg)) -sin(deg2rad(deg)); 
		sin(deg2rad(deg)) cos(deg2rad(deg))];

	T = [R t; 0 0 1];
end
