classdef ICP < matlab.mixin.Copyable
	%ICP is the iterative closest point algorithm
	%   Choosing and configuration of the ICP for point cloud
	%	allignment.
	
	properties (SetAccess = private)
		metric
		iterations
		pointDecimation
		maxDistance
		minDistance
		maxAngularDistance 
		minAngularDistance
        distanceReduction
		angDistanceReduction
        
    end

    properties (SetAccess = public)
        plotIterations
    end
    
	methods
		function obj = ICP(cfg)
			if isfield(cfg,'metric');obj.metric = cfg.metric;
			else obj.metric = 'point2point';end	
			if isfield(cfg,'iterations');obj.iterations = cfg.iterations;
			else obj.iterations = 100;end				
			if isfield(cfg,'pointDecimation');obj.pointDecimation = cfg.pointDecimation;
			else obj.pointDecimation = 1;end
			if isfield(cfg,'maxDistance');obj.maxDistance = cfg.maxDistance;
			else obj.maxDistance = 1e100;end
			if isfield(cfg,'minDistance');obj.minDistance = cfg.minDistance;
			else obj.minDistance = 1e100;end
			if isfield(cfg,'maxAngularDistance');obj.maxAngularDistance = cfg.maxAngularDistance;
			else obj.maxAngularDistance = 1e100;end		
			if isfield(cfg,'minAngularDistance');obj.minAngularDistance = cfg.minAngularDistance;
			else obj.minAngularDistance = 1e100;end					

			obj.distanceReduction = 1 - nthroot(obj.minDistance/obj.maxDistance,obj.iterations);
			obj.angDistanceReduction = 1 - nthroot(obj.minAngularDistance/obj.maxAngularDistance,obj.iterations);
            
            obj.plotIterations = false;
            
            clear plot_iterations;
		end
		
		function [TH,ER] = icp(obj,q,p)
		%ICP performs the Iterative Closest Point algorithm on two dimensional point
		% clouds.
		%
		% SYNOPSIS: [TH,ER] = icp(obj,q,p)   returns the homogeneous matrix TH
		% that minimizes the distances from (TR*p+TT) to q.
		% p is a 2xm matrix and q is a 2xn matrix. Also returns the end RMSE ER
		% of the error minimization.
		%
		% Martin Kjer and Jakob Wilm, Technical University of Denmark,2012
		% Modified by Alex Abich, Ulm University of Applied Sciences 2016		
		
			if size(q,1) ~= 2 || size(p,1) ~= 2
				error('Point clouds must have dimension 2xn!')
			end
			
			% Save OBJ because changing some values in methods
			tmp = copy(obj);
			
			Np = size(p,2);

			% Transformed data point cloud
			pt = p;

			% Allocate vector for RMS of errors in every iteration.
			ER = zeros(obj.iterations+1,1); 

			% Initialize total transform vector(s) and rotation matric(es).
			TT = zeros(2,1,obj.iterations+1);
			TR = repmat(eye(2,2),[1,1,obj.iterations+1]);

			% A kD tree should be built (req. Stat. TB >= 7.3)
			kdOBJ = KDTreeSearcher(transpose(q));

			if strcmp(obj.metric,'point2plane')
				normals = ICP.lsqnormest(q,4);
			end
			
			% Go into main iteration loop
			for k=1:obj.iterations

				pt_idx = 1:obj.pointDecimation:Np;

				% Do matching
				[q_idx,mindist] = ICP.match_kDtree(q,pt(:,pt_idx),kdOBJ);

				% Delete point matches which are likely false i.e. out of angular and
				% translational range
				[q_idx,pt_idx,mindist] = tmp.rm_out_of_distance(q_idx,pt_idx,pt,mindist);
				
				% Prepare weights for error minimization and remove small
				% weight correspondences
				[weight,q_idx,pt_idx,mindist] = ICP.weighting(q,q_idx,pt,pt_idx,mindist);

				% Minimize error
				if strcmp(obj.metric,'point2point')
					[R,T] = ICP.eq_point(q(:,q_idx),pt(:,pt_idx),weight);
				elseif strcmp(obj.metric,'point2plane')
					[R,T] = ICP.eq_plane(q(:,q_idx),pt(:,pt_idx),normals(:,q_idx),weight);
				end

				% Add to the total transformation
				TR(:,:,k+1) = R*TR(:,:,k);
				TT(:,:,k+1) = R*TT(:,:,k)+T;

				% Apply last transformation
				pt = TR(:,:,k+1)*p+repmat(TT(:,:,k+1),1,Np);

				% Root mean of objective function 
				if k == 1
					ER(k) = sqrt(sum(mindist.^2)/length(mindist));
				end
				ER(k+1) = ICP.rms_error(q(:,q_idx),pt(:,pt_idx));

				% If seems to have converged then trim matrices/vectors
				err_diff = ER(k+1)-ER(k);
				if abs(err_diff) < 1e-7 || err_diff > 0     % Magic number 1e-7 is taken as a small change of error (epsilon value)
					TR = TR(:,:,1:k+1);
					TT = TT(:,:,1:k+1);
					ER = ER(1:k+1);
					break;
                end
                
                if obj.plotIterations
                    ICP.plot_iteration(q',pt',q_idx,pt_idx,k);
                    pause(0.3);
                end
			end

			TR = TR(:,:,end);
			TT = TT(:,:,end);

			TH = [TR TT;0 0 1];
			ER = ER(end);
		end
	end
	
	methods (Static)		
		function [weight,q_idx,pt_idx,mindist] = weighting(q,q_idx,pt,pt_idx,mindist)
		%WEIGHTING computes weights of point correspondences
			weight_rot = ICP.rotation_weight(q(:,q_idx),pt(:,pt_idx));
			weight_trans = ICP.distance_weight(mindist);
			weight = weight_rot.*weight_trans;
			% Remove correspondences with small weights, i.e. likely outliers (big performance 
			% boost without loss of accuracy)
			rm_idx = weight < 1e-3;
			weight(rm_idx) = [];q_idx(rm_idx) = [];
			mindist(rm_idx) = [];pt_idx(rm_idx) = [];		
		end
			
		function weight = rotation_weight(q_corres,pt_corres)
		%ROTATION_WEIGHT does get weights of point correspondences using
		%their distance ratios.
			q_m = bsxfun(@minus,q_corres,mean(q_corres));
			pt_m = bsxfun(@minus,pt_corres,mean(pt_corres));
			pt_mag = sqrt(sum(pt_m.^2,1));
			dist_mag = sqrt(sum((pt_m - q_m).^2,1));

			dist_ratio = dist_mag./(pt_mag+1e-6);
			dist_ratio = dist_ratio./sum(dist_ratio);
			% What is a normal/likely occurring ratio 
			dist_med = median(dist_ratio);	

			% Those ratios that do not seem to be normal/likely should have
			% a small weight
			weight = 1-abs(dist_ratio-dist_med)./(dist_ratio+dist_med+1e-6);
		end
		
		function weight = distance_weight(mindist) 
		%DISTANCE_WEIGHT does get weights of point correspondences using their
		%distance.
			mindist = mindist./sum(mindist);
			m = mean(mindist);
			weight = transpose(1-abs(mindist-m)./(mindist+m+1e-6));
		end
		
		function n = lsqnormest(p, k)
		%LSQNORMEST estimates the normal 2D vector using K nearest
		%neighbours
			m = size(p,2);
			n = zeros(2,m);

			neighbors = transpose(knnsearch(transpose(p), transpose(p), 'k', k+1));

			for i = 1:m
				x = p(:,neighbors(2:end, i));
				p_bar = 1/k * sum(x,2);

				P = (x - repmat(p_bar,1,k)) * transpose(x - repmat(p_bar,1,k)); %spd matrix P
				%P = 2*cov(x);

				[V,D] = eig(P);

				[~, idx] = min(diag(D)); % choses the smallest eigenvalue

				n(:,i) = V(:,idx);   % returns the corresponding eigenvector    
			end
		end
		
		function [R,T] = eq_plane(q,p,n,weights)		
		%EQ_PLANE is the point to plane error optimization
			n = n .* repmat(weights,2,1);
			c = cross([p;zeros(size(weights))],[n;zeros(size(weights))]);

			cn = vertcat(c(3,:),n);
			C = cn*transpose(cn);

			b = - [sum(sum((p-q).*repmat(cn(1,:),2,1).*n));
				   sum(sum((p-q).*repmat(cn(2,:),2,1).*n));
				   sum(sum((p-q).*repmat(cn(3,:),2,1).*n))];

			X = pinv(C)*b;

			cz = cos(X(1)); 
			sz = sin(X(1)); 

			R = [cz cz-sz;
				 sz cz+sz];

			T = X(2:3);		
		end

		function [match,mindist] = match_kDtree(~,p,kdOBJ)
		%MATCH_KDTREE searches the KDOBJ for the closest points to each point in p
			[match,mindist] = knnsearch(kdOBJ,transpose(p));
			match = transpose(match);		
		end
		
		function [R,T] = eq_point(q,p,weights)
		%EQ_POINT is an error minimizing function of P to Q using weights for each
		%point in P.
			m = size(p,2);
			n = size(q,2);

			% normalize weights
			weights = weights ./ sum(weights);

			% find data centroid and deviations from centroid
			q_bar = q*transpose(weights);
			q_mark = q-repmat(q_bar,1,n);
			% Apply weights
			q_mark = q_mark .* repmat(weights,2,1);

			% find data centroid and deviations from centroid
			p_bar = p*transpose(weights);
			p_mark = p-repmat(p_bar,1,m);
			% Apply weights
			%p_mark = p_mark .* repmat(weights,3,1);

			N = p_mark*transpose(q_mark); % taking points of q in matched order

			[U,~,V] = svd(N); % singular value decomposition

			R = V*diag([1 det(U*V')])*transpose(U);

			T = q_bar-R*p_bar;
		end
		
		function ER = rms_error(p1,p2)
		%RMS_ERROR computes the root-mean-square error
		% Determine the RMS error between two equally sized point clouds with
		% point correspondance.
		% ER = rms_error(p1,p2) where p1 and p2 are 2xn matrices.
		%
			dsq = sum(power(p1-p2,2),1);
			ER = sqrt(mean(dsq));
        end		
        
        function plot_iteration(q,pt,q_idx,pt_idx,iter)
           	persistent fig fgm tit;
            if isempty(fig)
                fig = figure(10);
                fig.Name = 'ICP';
                clf(fig);
                hold on;
                fgm = cell(4,1);
                tit = title(['Iteration: ' int2str(iter)]);
            end
            
            set(groot,'CurrentFigure',fig);
            delete([fgm{1:end}]);
            tit.String = ['Iteration: ' int2str(iter)];
            fgm{1} = plot(q(:,1),q(:,2),'bo');
            fgm{2} = plot(pt(:,1),pt(:,2),'ro');
            fgm{3} = plot(q(q_idx,1),q(q_idx,2),'b+');
            fgm{4} = plot(pt(pt_idx,1),pt(pt_idx,2),'r+'); 

            axis equal;
           	drawnow;
        end        
	end
	
	methods (Access = private)
		function [q_idx,pt_idx,mindist] = rm_out_of_distance(obj,q_idx,pt_idx,pt,mindist)
		%RM_OUT_OF_DISTANCE takes thresholds for angular and translational distance into account to
		%remove unlikely point correspondences.
			if obj.distanceReduction > 0
				pt_mag = sqrt(sum(pt(:,pt_idx).^2,1));
				angdist = deg2rad(obj.maxAngularDistance)*pt_mag';
				maxdist = angdist+obj.maxDistance;
				rm_idx = find(mindist > maxdist);
				q_idx(rm_idx) = [];
				mindist(rm_idx) = [];
				pt_idx(rm_idx) = [];

				if obj.maxAngularDistance > obj.minAngularDistance
					obj.maxAngularDistance = obj.maxAngularDistance*(1-obj.angDistanceReduction);
				else
					obj.maxAngularDistance = obj.minAngularDistance;
				end
				if obj.maxDistance > obj.minDistance
					obj.maxDistance = obj.maxDistance*(1-obj.distanceReduction);
				else
					obj.maxDistance = obj.minDistance;			
				end
			end
        end
		
	end
end

