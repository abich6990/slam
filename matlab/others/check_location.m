function [locationCorrect,expState] = check_location(numLoc,curState,testCfg,filename)
%CHECK_LOCATION Summary of this function goes here
%   Detailed explanation goes here
    persistent location errRad;
	
	if isempty(location) && exist('filename','var') && exist('testCfg','var')
		fileID = fopen(filename);
		
		if fileID > 0
			data = textscan(fileID,'%f:%f:%f');
			data = cell2mat(data);
			fclose(fileID);

			location = data;
			errRad = testCfg.stateErrRad;
		else
			location = [];
		end
	end
	
	if isempty(location)
		locationCorrect = true;
		expState = curState;
		return;
	end
	
	expState = location(numLoc,:);
	diffCurState = abs(expState - curState);
	radius = norm(diffCurState(1:2));
	
	% If no state was specified then it is always true
	if isnan(diffCurState)
		locationCorrect = true;
		return;
	end
	
	locationCorrect = radius <= errRad;
	
	if ~locationCorrect
		disp([char(9) 'curState: ' char(9) num2str(curState)]);
		disp([char(9) 'expState: ' char(9) num2str(expState)]);
		disp([char(9) 'Max allowed error radius: ' num2str(errRad) ' but has ' num2str(radius)]);
	end
	
end

