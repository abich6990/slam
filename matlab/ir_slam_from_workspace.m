function ir_slam_from_workspace(varargin)
%IR_SLAM_FROM_WORKSPACE is useful when running IR_SLAM until before a
%   point of error occures saving the workspace. Now instead of rerunning
%   all calculations until the point of error it is possible to start 
%   from the saved state and check for the error without worrying, that
%   a error could stop the program.
	%% SLAM main loop

	% Add paths of subfolders for usage of functions
	addpath(genpath(pwd));
	% Clear persistent variables in functions
	clear show_gridmap;clear plot_state;
	clear check_location;clear time_left_until_done;    
    
    load('/tmp/workspace');
	% Every iteration of the main loop one new scan is used
	% i.e. Observation of the next timestep
    load_scan(arg.numStart,scanFile,arg.configpath);
     
%     scnScn.plotIterations = true;
%     scnMap.plotIterations = true;
    
    arg.numStart = numScan - 1;
    
	for numScan=arg.numStart+1:arg.numEnd

        % Initialize observation for this iteration of the main loop
		tic;	% Start estimating time left until main loop done
		iter = numScan - arg.numStart + 1;	% Use for relative instead absolute indexing
        locScan = load_scan(numScan);

		if testCfg.runSpeed > 0
            if ~exist('sld', 'var')
           		sld = uislider('Limits',[1 100],'Value',testCfg.gridmapDecimation); % Runtime changing visualization update rate of map
            end
			testCfg.gridmapDecimation = round(sld.Value);
		end
        %% State Estimation (Rotation,Translation)    

        % Estimate motion from last and current/local scan i.e. use scan-matching as replacement
        % for odometry of wheels
		odoT = scnScn.icp(lstScan',locScan');
		dOdoState = affine2xytheta(odoT);
		dOdoState(1:2) = transform(dOdoState(1:2),[0 0 curState(3)]);	% Rotate translation from local to world coordinate system
   
        % Transform to (approximated) world coordinate system (gridmap)
        % via usage of the last state and scan-matching
        % result, then estimate state correction by allignment to gridmap
		locScanTrans = transform(locScan,curState+dOdoState);
		[xLim,yLim] = gridmap.mapLimit(locScanTrans);
		wndMap = gridmap.cellInRectangle(xLim,yLim);	% Reduce points to a possible box area instead of whole global map
		alnT = scnMap.icp(wndMap',locScanTrans'); 
		dAlnState = affine2xytheta(alnT);
		
        %% State Update

        dState = dOdoState+dAlnState;	% Estimated state change from last to current state
		% Do only trust Scan-Matching odometry if DSTATE seems to be to big 
		if norm(dState(1:2)) > slamcfg.maxDeltaTranslation || abs(dState(3)) > slamcfg.maxDeltaRotation
			dState = dOdoState;
		end
        curState = curState+dState;

        %% Map Update

        % Save the current as last scan for scan-matching in the next iteration
        lstScan = locScan;
		% Save states for visualization of robot trajectory. Not needed in
		% C/C++ implementation because only implementing 'Online SLAM'
		trajectory(iter,:) = curState;
		dTrajectory(iter,:) = [dOdoState,dAlnState];
	
		locScanTrans = transform(locScan,curState);

		try
			[freeCell] = gridmap.update(locScanTrans,curState(1:2));
		catch ME
			visualization(curState,dState,locScanTrans,gridmap,trajectory,iter,numScan,arg,freeCell,dTrajectory,testCfg,true);
			warning('Unexpected error in GRIDMAP.UPDATE occured!');
			rethrow(ME);
		end
		
		%% Visualization and checks for validity of estimated state and map. Also runtime checks		
		
		% Only visualize every TESTCFG.GRIDMAPDECIMATION updated map
		visualization(curState,dState,locScanTrans,gridmap,trajectory,iter,numScan,arg,freeCell,dTrajectory,testCfg);                		
% 		[~,runtime] = time_left_until_done(numScan,iter);
		
		if ~check_location(numScan,curState)
			disp('Stopping main loop because of location error!');
			break;
		end
		
	end		% Set here conditional breakpoint if analyzing specific iteration
    disp('SLAM main loop done!');
	%% Some additional work after end of main loop
	
    disp('Additional work start!');
    
	disp(['Time average per iteration: ' datestr(runtime/(numScan-arg.numStart)/(24*60*60),'SS.FFF') ' seconds']);
	save(fullfile(arg.savepath,'workspace'));
	if isnan(getkeywait(10,'Do you want to see a playback (KEY or SPACE)? You have 10 seconds to press a KEY'))
		disp_state(curState,dState,numScan,arg);	
		playback('workspaceFile',fullfile(arg.savepath,'workspace'));	% Does need a saved workspace for playback
	else
		visualization(curState,dState,locScanTrans,gridmap,trajectory,iter,numScan,arg,freeCell,dTrajectory,testCfg,true);
	end

    disp('Additional work done!');
	
end



function visualization(curState,dState,locScanTrans,gridmap,trajectory,iter,numScan,arg,freeCell,dTrajectory,testCfg,forceVisu)
	if testCfg.runSpeed == 2
		if mod(iter,testCfg.gridmapDecimation) == 0
			disp_state(curState,dState,numScan,arg);
			show_gridmap(locScanTrans,gridmap,trajectory(1:iter,:),numScan,arg.numEnd,freeCell,true);
			plot_state(dTrajectory,numScan,numScan);
		end
	elseif testCfg.runSpeed == 1
		if mod(iter,testCfg.gridmapDecimation) == 0
			show_gridmap(locScanTrans,gridmap,trajectory(1:iter,:),numScan,arg.numEnd,freeCell,false);
		end	
	end
	if exist('forceVisu','var') && forceVisu
		disp_state(curState,dState,numScan,arg);
		show_gridmap(locScanTrans,gridmap,trajectory(1:iter,:),numScan,arg.numEnd,freeCell,true);
		plot_state(dTrajectory,numScan,inf);		
	end
end

function ch = getkeywait(m,msg) 
% GETKEYWAIT - get a key within a time limit
%   CH = GETKEYWAIT(P) waits for a keypress for a maximum of P seconds. P
%   should be a positive number. CH is a double representing the key
%   pressed key as an ascii number, including backspace (8), space (32),
%   enter (13), etc. If a non-ascii key (Ctrl, Alt, etc.) is pressed, CH
%   will be NaN.
%   If no key is pressed within P seconds, -1 is returned, and if something
%   went wrong during excution 0 is returned. 
%
%  See also INPUT,
%           GETKEY (FileExchange)

% tested for Matlab 6.5 and higher
% version 2.1 (jan 2012)
% author : Jos van der Geest
% email  : jos@jasen.nl

% History
% 1.0 (2005) creation
% 2.0 (apr 2009) - expanded error check on input argument, changed return
% values when a non-ascii was pressed (now NaN), or when something went
% wrong (now 0); added comments ; slight change in coding
% 2.1 (jan 2012) - modified a few properties, included check is figure
%                  still exists (after comment on GETKEY on FEX by Andrew). 

% check input argument
	if numel(m)~=1 || ~isnumeric(m) || ~isfinite(m) || m <= 0,    
		error('Argument should be a single positive number.') ;
	end

	disp(msg);
	
	% set up the timer
	tt = timer ;
	tt.timerfcn = 'uiresume' ;
	tt.startdelay = m ;            

	% Set up the figure
	% May be the position property should be individually tweaked to avoid visibility
	callstr = 'set(gcbf,''Userdata'',double(get(gcbf,''Currentcharacter''))) ; uiresume ' ;
	fh = figure(...
		'name','Press a key', ...
		'keypressfcn',callstr, ...
		'windowstyle','modal',... 
		'numbertitle','off', ...
		'position',[0 0  1 1],...
		'userdata',-1) ; 
	try
		% Wait for something to happen or the timer to run out
		start(tt) ;    
		uiwait ;
		ch = get(fh,'Userdata') ;
		if isempty(ch), % a non-ascii key was pressed, return a NaN
			ch = NaN ;
		end
	catch
		% Something went wrong, return zero.
		ch = 0 ;
	end

	% clean up the timer ...
	stop(tt) ;
	delete(tt) ; 
	% ... and figure
	if ishandle(fh)
		delete(fh) ;
	end
end
