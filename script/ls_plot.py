'''
Documentation, License etc.

@package vgm_plot
'''

delimiter = ' '

import time
import sys, re
import numpy as np
import matplotlib.pyplot as plt


def main():
	fig = plt.figure()

	angle_offset = 90 # rotate to y-axis being the front instead of x-axis
	decimation = 1 # plot every DECIMATION scan
	scale = (-30, 30, -25, 30) # axis scaling
	count = 0
	
	ax = fig.add_subplot(111)
	ax.set_xscale('linear')
	ax.set_yscale('linear')
	ax.grid(True)
	ax.axis(scale)

	l1, = ax.plot([], 'ro')
	fig.show()

	while True:
		start = time.time()

		for i in range(0, decimation):
			try:
				line = sys.stdin.readline().rstrip()

			except KeyboardInterrupt:
				break

			if not line:
				time.sleep(1)
				continue

		records = [record.split(':') for record in line.split(delimiter)]
		distances = [float(record[0]) for record in records]
		angles = np.array([float(record[1]) for record in records])

		angles = np.deg2rad(angles + angle_offset)
		
		l1.set_xdata(distances*np.cos(angles))
		l1.set_ydata(distances*np.sin(angles))

		#  ax.relim()	# if fix scale is desired then leave commented
		#  ax.autoscale()	# if fix scale is desired then leave commented

		count = count + decimation
		
		plt.title(count)
		plt.draw()
		fig.canvas.flush_events()

		end = time.time()
		print count,"{0:.3f}".format(end - start),"{0:.2f}".format(sum(distance > 29.0 for distance in distances)/float(len(distances)))

	return 0

if __name__ == '__main__':
	sys.exit(main())
