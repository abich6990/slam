#!/usr/bin/awk -f


BEGIN {
	maxrange = 29.0

	fieldsep = ":"
	recordsep = " "
}


{
	for(idx=1; idx < NF; idx++){
		split($idx,fields,fieldsep,seps)
		
		if(fields[1] < maxrange)
			printf("%f%s%f%s", fields[1], fieldsep, fields[2], recordsep)
	}

	split($NF,fields,fieldsep,seps)
	
	if(fields[1] < maxrange)
		printf("%f%s%f", fields[1], fieldsep, fields[2])
	
	printf("\n")
}
