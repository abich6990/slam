#!/usr/bin/awk -f


BEGIN {
	pi = atan2(0, -1)
	deg2rad = pi/180

	fieldsep = ":"
	recordsep = " "
}


{
	for(idx=1; idx < NF; idx++){
		split($idx,fields,fieldsep,seps)
		
		x = fields[1]*cos(fields[2]*deg2rad)
		y = fields[1]*sin(fields[2]*deg2rad)

		printf("%f%s%f%s", x, fieldsep, y, recordsep)
	}

	split($NF,fields,fieldsep,seps)
	
	x = fields[1]*cos(fields[2]*deg2rad)
	y = fields[1]*sin(fields[2]*deg2rad)

	printf("%f%s%f", x, fieldsep, y)
	
	printf("\n")
}
