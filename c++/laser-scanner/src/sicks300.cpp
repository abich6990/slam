/*
 * sicks300.cpp
 * 
 * Copyright 2017 Alex Abich <abich90@hotmail.de>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include "sicks300.hpp"
#include "message.hpp"

#include "returnstatus.hpp"

#include <fcntl.h>	// Flags for open call in Constructor
#include <termios.h>	// Init of filedescriptor in Constructor
#include <unistd.h>	// For close
#include <sys/ioctl.h>		// Checking S300 buffer via ioctl()

#include <cstring>
#include <iostream>
#include <iomanip> // setprecision
#include <sstream> // stringstream

#define DEVICE std::string("SickS300")
#define ERROR std::string("error")
#define DEVICE_FUNCTION_ERROR std::string(DEVICE + " " +  __FUNCTION__ + " " + ERROR)


SickS300::SickS300(std::string &devPath)
	: LaserScanner(), readings(MSG_READINGS), msg(*(new Message))
{
	set_format("%r:%a");
	if(connect(devPath)) std::exit(EXIT_FAILURE);
}


SickS300::~SickS300()
{
	disconnect();
	delete &msg;
}


int SickS300::connect(std::string &devPath)
{
	int openFlag = O_RDWR | O_NOCTTY;
	tcflag_t cCflag = B460800 | CS8 | CLOCAL | CREAD;

	uart = ::open(devPath.c_str(), openFlag);

	if(uart < 0) goto out;

	struct termios uartOpt;

	tcgetattr(uart, &uartOpt);

	uartOpt.c_cflag = cCflag;
	uartOpt.c_iflag = IGNPAR;
	uartOpt.c_oflag = 0;
	uartOpt.c_lflag = 0;
	uartOpt.c_cc[VMIN] = 0;
	uartOpt.c_cc[VTIME] = 5;

	if(tcsetattr(uart, TCSANOW, &uartOpt)) goto out;
	
	tcflush(uart, TCIOFLUSH);
	usleep(1000);
	tcflush(uart, TCIFLUSH);
	
	return RET_OK;

out:
	std::perror(std::string(DEVICE_FUNCTION_ERROR + " filename '" + devPath + "'").c_str());
	return RET_HARD_ERR;
}


void SickS300::disconnect()
{
	if(uart >= 0) ::close(uart);	// Only close if a valid FD available
	uart = -1;
}


int SickS300::read()
{
	uint8_t *ptrMsg = (uint8_t*) &msg;	// Handle MSG like a byte-array

	int missByte = MSG_BYTES;

	while(missByte){	// Read until MSG complete or error occurred
		int readByte = ::read(uart, ptrMsg, missByte);

		if(readByte <= 0){
			std::perror(DEVICE_FUNCTION_ERROR.c_str());
			return RET_SOFT_ERR;
		}

		missByte -= readByte;
		ptrMsg += readByte;
		
		// Check available size of bytes in buffer of S300
		int bytesBuf = checkBuffer();

		if(bytesBuf < 0){
			std::perror(DEVICE_FUNCTION_ERROR.c_str());
			return RET_SOFT_ERR;
		}
		/* Give buffer time to collect readings. Kind of a hack because polling
		 * for data, but works. Maybe using interrupts could fix this
		 */
		if(bytesBuf < missByte)
			usleep(10 * 1000);	
	}
	
	return (syncMessage() || crcCheck())	? RET_SOFT_ERR
											: RET_OK;
}


int SickS300::write()
{
	/* Default records are printed as "RANGE_0:PHI_0 RANGE_1:PHI_1 ... RANGE_N:PHI_N",
	 * where "RANGE_I:PHI_I" is one record, consisting of the range-value in
	 * meter and its corresponding angular-value in degree
	 */
	if(to_string(0).empty())
		return RET_HARD_ERR;
	 
	for(size_t i = 0; i < readings - 1; i++)
		std::cout << to_string(i) << " ";
	
	// Flush output after last record
	std::cout << to_string(readings - 1) << std::endl;
	
	return RET_OK;
}


int SickS300::syncMessage()
{
	uint8_t *ptrMsg = (uint8_t*) &msg;

	// Search for a recognisable bytearray (i.e. SYNC_N consecutive SYNC_HEX)
	int syncIdx = findSyncIdx();	

	// Invalid because bytearray not found (unlikely). Return failure
	if(syncIdx < 0) return RET_SOFT_ERR;
	
	int syncOffset = syncIdx - SYNC_TO_BYTE_INDEX;
	
	// Already synchronized (likely). Return success
	if(!syncOffset) return RET_OK;
	// Invalid because header incomplete (unlikely). Return failure	
	if(syncOffset < 0) return RET_SOFT_ERR;
		
	// Need synchronization. Continue synchronization
	uint8_t *src = ptrMsg + syncOffset;	// Set SRC on beginning of valid data
	std::memmove(ptrMsg, src, MSG_BYTES - syncOffset);	// Move data to start

	// Read missing bytes into MSG
	uint8_t *dst = ptrMsg + MSG_BYTES - syncOffset;
	size_t missByte = syncOffset;

	while(missByte){	
		int readByte = ::read(uart, dst, missByte);

		if(readByte <= 0){
			std::perror(DEVICE_FUNCTION_ERROR.c_str());
			return RET_SOFT_ERR;
		}

		missByte -= readByte;
	}

	return RET_OK;	
}


int SickS300::crcCheck()
{
	uint16_t crc16 = 0xFFFF;

	/* Check from 5. byte of MSG (in HEADER field) to last DATA field byte (Byte
	 * before CRC field begins). This algorithm is taken straight out of the
	 * file "Telegram listing", which is available at the manufactures website
	 */
	for(size_t i = 0; i < MSG_BYTES - HDR_REPLY - MSG_CRC; i++){
		crc16 = (crc16 << 8) ^ (crcTable[(crc16 >> 8) ^
			(((uint8_t*) &msg.header.block_nr) [i])]);
	}

	return (crc16 != msg.crc.value)	? RET_SOFT_ERR
									: RET_OK;
}


float SickS300::range(int dataIdx)
{
	return msg.data[dataIdx].distance/100.0;	// cm to meter
}


float SickS300::phi(int dataIdx)
{
	// Division by 2 is for getting the half of total readings
	float middle = MSG_READINGS >> 1;
	float degInc = 1.0 / MSG_DATA_READ_PER_DEG;

	return degInc*(dataIdx - middle);	
}


int SickS300::status(int dataIdx)
{
	/* Beginning from LSB and if bit set:
	 * reflector recognized, reading in safe zone, reading in warning field
	 */
	return msg.data[dataIdx].status_bits;	
}


std::string SickS300::to_string(int dataIdx)
{
	// Only implement %r, %a, %s and %i formats
	 std::stringstream record;
	 std::string format = get_format();

	for(size_t it = 0; it != format.length(); ++it) {
		if(format.at(it) == '%')
			switch(format.at(++it)){
				case 'r':
					record << std::fixed << std::setprecision(2) << range(dataIdx);
					break;
					
				case 'a':
					record << std::fixed << std::setprecision(1) << phi(dataIdx);
					break;
					
				case 's':
					record << std::to_string(status(dataIdx));
					break;
					
				case 'i':
					record << std::to_string(dataIdx + 1); // Count from 1 not 0
					break;

				default:
					std::cerr << "Format string '%" << format.at(it) << "' not valid!" << std::endl;
				
					return "";						
			}
		else
			record << format.at(it);
	}

	return record.str();
}


int SickS300::checkBuffer()
{
	int bytes = RET_SOFT_ERR;	// Set to invalid/negativ value
	
	return (ioctl(uart, FIONREAD, &bytes) < 0)	? RET_SOFT_ERR 
												: bytes;	
}


int SickS300::findSyncIdx()
{
	uint8_t bytearray[SYNC_N];

	std::memset(bytearray, SYNC_HEX, SYNC_N);

	uint8_t *arr = (uint8_t*) &msg;
	uint8_t *start = arr;
	int offset = RET_SOFT_ERR;	// Set to invalid/negativ value
	
	for(size_t i = 0; i < MSG_BYTES; i++){
		// If string found return offset from start address
		if(!std::memcmp(bytearray, arr, SYNC_N)){
			offset = (int) (arr - start);
			break;
		}

		arr++;
	}
	
	return offset;	
}


