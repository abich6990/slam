/*
 * mainLaserScanner.cpp
 * 
 * Copyright 2017 Alex Abich <abich90@hotmail.de>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include "laserscanner.hpp"	// Interface class for a laser-scanner
#include "sicks300.hpp"	// This is the implemented LASERSCANNER

#include "options.hpp"	// Configuration and command line. OPTIONS for MAIN
//~ #include <chrono>

int main(int argc, char *argv[])
{
	getOptions(argc, argv);	// If default settings need change via command-line
	
	LaserScanner &ls = *(new SickS300(options.devPath));

	if(!options.write_format.empty())
		ls.set_format(options.write_format);
	
	int failRead = 0;
	const int failMax = 3;
	
	signal(SIGINT, &sigHandler);
	while(running){
	    //~ using namespace std::chrono;
		//~ high_resolution_clock::time_point t1 = high_resolution_clock::now();
		    
		if(ls.read() < 0){
			std::cerr << "Reading data from Laser-Scanner failed!" << std::endl;
			
			// Reconnect if failed to read FAILMAX times
			if(++failRead == failMax){
				failRead = 0;
				std::cerr << "Reconnect Laser-Scanner!" << std::endl;
				
				ls.disconnect();
				// Exit if reconnect failed
				if(ls.connect(options.devPath)){
					std::cerr << "Reconnect failed!" << std::endl;
					break;
				} 
			}
			
			continue;
		}
		
		if(ls.write() < 0){
			std::cerr << "Writing data to output failed!" << std::endl;
			break;		
		}
		
		//~ high_resolution_clock::time_point t2 = high_resolution_clock::now();
		//~ duration<double> time_span = duration_cast<duration<double>>(t2 - t1);

		//~ std::cerr << time_span.count() << std::endl;
	}

	delete &ls;
	return 0;
}






