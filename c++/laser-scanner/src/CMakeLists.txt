cmake_minimum_required (VERSION 3.1)
project (laser-scanner)

file (GLOB_RECURSE SRC "${SRC_SRC_DIR}/*.cpp")
file (GLOB_RECURSE INC "${INC_SRC_DIR}/*.hpp")

message (${INC})
message (${SRC})

add_executable (${PROJECT_NAME} "${SRC}" "${INC}")

set (CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -g -Wall -std=c++11")
set (CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wall -std=c++11")

set (LIBRARIES ${LIBRARIES})

target_include_directories (${PROJECT_NAME} PUBLIC ${INCLUDE_DIRS})
target_link_libraries (${PROJECT_NAME} ${LIBRARIES})

set_target_properties (${PROJECT_NAME} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${BIN_DIR})
set_target_properties (${PROJECT_NAME} PROPERTIES OUTPUT_NAME ${PROJECT_NAME})

install (TARGETS ${PROJECT_NAME} RUNTIME DESTINATION ${BIN_DIR})
