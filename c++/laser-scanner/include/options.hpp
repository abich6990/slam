/*
 * options.hpp
 * 
 * Copyright 2017 Alex Abich <abich90@hotmail.de>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#ifndef OPTIONS_HPP
#define OPTIONS_HPP

#include <signal.h>	// Signal handling for while loop abortion.
#include <getopt.h>	// For reading command-line arguments
#include <iostream>	// Output to cerr


static sig_atomic_t volatile running = 1;

void getOptions(int argc, char *argv[]);
void showUsage(std::string name);
void sigHandler(int signum);


struct Options{
	std::string devPath;
	std::string write_format;
} options;


void getOptions(int argc, char *argv[])
{
	// Set default OPTIONS
	options.devPath = "/dev/ttyUSB0";
	// OPTIONS.WRITE_FORMAT no initialization to use laser-scanners hard coded default
	
	int opt = 0, optIdx = 0;

	static struct option long_options[] = {
		{"device", 	required_argument, 	0, 	'd'},
		{"format",	required_argument,	0,	'f'},
		{"help", 	no_argument, 		0, 	'h'},
		{0, 		0, 					0, 	0}
	};
	
	while((opt = getopt_long(argc, argv, "d:f:h", long_options, &optIdx)) != -1)
		switch(opt){
			case 'd':
				options.devPath = optarg;
				break;

			case 'f':
				options.write_format = optarg;
				break;
			
			case 'h':
				showUsage(argv[0]);
				exit(EXIT_SUCCESS);
				break;

			case '?':
				// getopt_long already printed an error message
				showUsage(argv[0]);
				exit(EXIT_FAILURE);
				break;
				
			default:
				abort();	// cannot be reached
		}

	for(int i = optind; i < argc; ++i)
		std::cerr << "Non-option argument " << argv[i] << std::endl;
}

void showUsage(std::string name)
{
    std::cerr << "Usage: " << name << " [OPTION]\n"
              << "Options:\n"
              << "\t-d DEVICENAME, --device=DEVICENAME\n"
              << 	"\t\tSpecify the device filename; default: " << options.devPath << "\n"
              << "\t-f FORMAT_STRING, --format=FORMAT_STRING\n"
              <<	"\t\tFormat for output; %r: range, %a: angle, "
				"%x: x-axis position, %y: y-axis position, %s: status, %i: index. "
				"BUT only %a and %r or %x and %y are mandatory!"
              << "\t-h, --help\n"
              << 	"\t\tShow this help message"
              << std::endl;
}

void sigHandler(int signum)
{
	if(signum == SIGINT)
		running = 0;
}

#endif /* OPTIONS_HPP */ 
