/*
 * sicks300.hpp
 * 
 * Copyright 2017 Alex Abich <abich90@hotmail.de>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#ifndef SICKS300_HPP
#define SICKS300_HPP

#include "laserscanner.hpp"

#include <string>

struct Message;	// Forward declaration for hiding implementation details

class SickS300: public LaserScanner
{
	public:
		SickS300(std::string &devPath);
		~SickS300() override;
		
		int connect(std::string &devPath) override;
		void disconnect() override;
		
		int read() override;
		int write() override;

		const size_t readings;
	
	private:
		Message &msg;
		int uart;
		
		int syncMessage();
		int crcCheck();
		
		int checkBuffer();		
		int findSyncIdx();

		float range(int dataIdx);
		float phi(int dataIdx);
		int status(int dataIdx);

		std::string to_string(int dataIdx);
};

#endif /* SICKS300_HPP */ 
