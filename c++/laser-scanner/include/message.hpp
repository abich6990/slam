/*
 * message.hpp
 * 
 * Copyright 2017 Alex Abich <abich90@hotmai.de>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#ifndef MESSAGE_HPP
#define MESSAGE_HPP

#include <cstdint>

/**
 * @brief The MSG_HELP enum has MESSAGE specific values,
 * which are used for defining how MESSAGE is build.
 */
enum MSG_HELP{
    MSG_BYTES = 1108,	///< Length of a struct MESSAGE in bytes.
    MSG_READINGS = 541,	///< Readings in one struct MESSAGE.
    MSG_HDR = 24,	///< Length of a struct Header object in bytes.
    MSG_DATA = 1082,	/**< Length of a struct MESSAGE data segment in
    bytes. This represents => (270° / 0.5°/readings) + 1 reading(0°) = 541
    => each reading has 2 Bytes length => 541 * 2 = 1082. */
    MSG_CRC = 2,	///< Length of a struct MESSAGE crc segment in bytes.
    MSG_DATA_READ_PER_DEG = 2	/**< Readings per degree. With this
    the current angle can be computed. */
};
/**
 * @brief The HEADER_HELP enum holds struct Header specific data,
 * for documentation and programming purposes.
 */
enum HEADER_HELP{
    HDR_REPLY = 4,	/**< Length of the struct Header reply member
    in bytes. */
    HDR_BLOCK = 2,	/**< Length of the struct Header block_nr member
    in bytes. */
	HDR_LENGTH = 2,	/**< Length of the struct Header length member
	in bytes. */
	HDR_COOR = 1,	/**< Length of the struct Header coor_flg member
	in bytes. */
	HDR_DEV_ADD = 1,	/**< Length of the struct Header dev_add member
	in bytes. */
	HDR_PROT = 2,	/**< Length of the struct Header prot_nr member
	in bytes. */
	HDR_STAT = 2,	/**< Length of the struct Header status member
	in bytes. */
	HDR_TIME = 4,	/**< Length of the struct Header timestamp
	member in bytes. */
	HDR_TEL_NR = 2,	/**< Length of the struct Header tel_nr
	member in bytes. */
	HDR_OUT = 2,	/**< Length of the struct Header output member
	in bytes. */
	HDR_ID_DATA = 2	/**< Length of the struct Header id_data
	member in bytes. */
};
/**
 * @brief The SYNC_HELP enum holds synchronization specific data.
 */
enum SYNC_HELP{
	SYNC_HEX = 0xBB,	/**< Byte-value to synchronize on. */
	SYNC_N = 2,	/**< Number of continuous SYNC_HEX. */
	SYNC_TO_BYTE_INDEX =
        (MSG_HDR - HDR_OUT - HDR_ID_DATA)	/**<
        Synchronization-index in MESSAGE (interpreted as a byte-array). */
};
//------------------------------------------------------------------------------
/**
 * @brief crcTable is the CRC table for the MESSAGE's Code Redundancy
 * Check.
 */
static uint16_t const crcTable[256] = {
	0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
	0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
	0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
	0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
	0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
	0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
	0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
	0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
	0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
	0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
	0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
	0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
	0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
	0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
	0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
	0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
	0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
	0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
	0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
	0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
	0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
	0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
	0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
	0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
	0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
	0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
	0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
	0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
	0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
	0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
	0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
	0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};
//------------------------------------------------------------------------------
/**
 * @brief The Header struct is representing the Header data of a
 * MESSAGE.
 */
struct Header{
	uint32_t reply;		///< Header#reply contains normally 0x00000000.
	uint16_t block_nr;	/**< Header#block_nr contains the data block number
	inc after triggered event. */
	uint16_t length;	/**< Header#length contains (MSG_BYTES - 4) / 2 as a
	2 Byte value = 0x0228 or 0x2802. */
	uint8_t coor_flg;	///< Header#coor_flg contains 0xFF or 0x00.
	uint8_t dev_add;		///< Header#dev_add contains 0x07.
	uint16_t prot_nr;	/**< Header#prot_nr contains the protocol version
	normally 0x0301 but also 0x0201. */
	uint16_t status;	/**< Header#status contains either
	normal 0x0000 or lockout 0x0001. */
	uint32_t timestamp;	/**< Header#timestamp contains a scannumber
	(timestamp). */
	uint16_t tel_nr;		///< Header#tel_nr contains a telegram number.
	uint16_t output;	/**< Header#output contains 0xBBBB if
	measurement output is set to distance. */
	uint16_t id_data;	///< Header#id_data contains 0x1111.
} __attribute__((packed));
/**
 * @brief The Data struct is representing one reading of the .
 * e.i. a distance and status bits. The angle can be computed via the
 * index of the reading in the Message's Data array.
 */
struct Data{
	uint16_t distance: 13;	/**< Data#distance is the measured distance in
	cm. */
	uint16_t status_bits: 3;		/**< Data#status_bits are one of the
	following, beginning from LSB and if bit set:
	reflector recognized, reading in safe zone,
	reading in warning field. */
} __attribute__((packed));
/**
 * @brief The CRC struct is representing the Cyclic Redundancy
 * Check of the struct Header, without the <B>reply</B> member and
 * the struct Data.
 */
struct CRC{
	uint16_t value;		/**< CRC#value holds the MSG_CRC bytes long CRC
	Code */
} __attribute__((packed));
//------------------------------------------------------------------------------
/**
 * @brief The Message struct is the in memory representation of a data
 * reading from the Laser-scanner. 
 */
struct Message{
	struct Header header;	/**< Message#header is a message header
	for one reading with the struct Header type. */
	struct Data data[MSG_READINGS];	/**< Message#data is a
	field of struct Data and each holds one measurement. */
	struct CRC crc;	/**< Message#crc is of the struct CRC type
	and holds a 16 bit Cyclic Redundancy Check value. */
} __attribute__((packed));

#endif /* MESSAGE_HPP */ 
