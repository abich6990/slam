/*
 * returnstatus.hpp
 * 
 * Copyright 2017 Alex Abich <abich90@hotmail.de>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#ifndef RETURNSTATUS_HPP
#define RETURNSTATUS_HPP

/* Do not change this values because it is assumed that succes indicates a zero 
 * and failure a negative return value. 
 */ 
enum RETURNSTATUS
{
	RET_OK = 0,	///< Return no error occured.
	RET_SOFT_ERR = -1,	///< Return non fatal error occured.
	RET_HARD_ERR = -2 	/**< Return a fatal error which indicates that program 
	needs to be stopped. */
};

#endif /* RETURNSTATUS_HPP */ 
