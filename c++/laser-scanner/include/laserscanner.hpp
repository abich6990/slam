/*
 * laserscanner.hpp
 * 
 * Copyright 2017 Alex Abich <abich90@hotmail.de>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#ifndef LASERSCANNER_HPP
#define LASERSCANNER_HPP

#include <string>

class LaserScanner
{
	public:
		LaserScanner(){};
		virtual ~LaserScanner(){};
	
		virtual int connect(std::string &devPath) = 0;
		virtual void disconnect() = 0;
	
		virtual int read() = 0;
		virtual int write() = 0;

		/* Allowed formats are:
		 * %r: range, %a: angle, %x: x-axis position, %y: y-axis position,
		 * %s: status, %i: index.
		 * BUT only %a and %r or %x and %y are mandatory. For non implemented
		 * formats give fatal error! Implement formats in WRITE
		 */
		void set_format(std::string new_format);
		std::string get_format();

	private:
		std::string format;
};

#endif /* LASERSCANNER_HPP */ 
