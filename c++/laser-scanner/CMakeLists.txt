cmake_minimum_required (VERSION 3.1)
project (laser-scanner)

set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -g -Wall -std=c++11")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wall -std=c++11")

set (INC_DIR "include")
set (BIN_DIR "bin")
set (LIB_DIR "lib")
set (ETC_DIR "etc") # Added but currently not used directory
set (DOC_DIR "doc")	# Added but currently not used directory
set (SRC_DIR "src")

set (INC_SRC_DIR "${CMAKE_SOURCE_DIR}/${INC_DIR}")
set (ETC_SRC_DIR "${CMAKE_SOURCE_DIR}/${ETC_DIR}")
set (DOC_SRC_DIR "${CMAKE_SOURCE_DIR}/${DOC_DIR}")
set (SRC_SRC_DIR "${CMAKE_SOURCE_DIR}/${SRC_DIR}")

include_directories (${INC_DIR})

add_subdirectory (src)
